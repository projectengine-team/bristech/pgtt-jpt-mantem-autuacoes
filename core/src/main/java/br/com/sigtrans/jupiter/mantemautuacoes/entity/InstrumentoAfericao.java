package br.com.sigtrans.jupiter.mantemautuacoes.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.bristech.msf.entity.TenantEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Entity
@Table(name = "JPT_INST_AFERICAO")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class InstrumentoAfericao extends TenantEntity {
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	@Column(name = "INSTRUMENTO", length = 128)
	private String instrumento;

	@Getter
	@Setter
	@Column(name = "DESCRICAO", length = 512)
	private String descricao;

	@Getter
	@Setter
	@Column(name = "DT_VALIDADE")
	@Temporal(TemporalType.DATE)
	private Date dtValidade;

}