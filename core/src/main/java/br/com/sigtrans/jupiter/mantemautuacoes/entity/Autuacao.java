package br.com.sigtrans.jupiter.mantemautuacoes.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.bristech.msf.common.entity.EmbeddedCoordenadas;
import br.com.bristech.msf.common.entity.EmbeddedEndereco;
import br.com.bristech.msf.entity.TenantEntity;
import br.com.sigtrans.jupiter.mantemautuacoes.enums.StatusAutuacao;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@ToString
@Table(name = "JPT_AUTUACAO")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Autuacao extends TenantEntity {
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	@Column(name = "ID_PERMISSAO", length = 64)
	private String permissaoId;

	@Getter
	@Setter
	@Column(name = "STATUS", length = 32)
	@Enumerated(EnumType.STRING)
	private StatusAutuacao status;

	@Getter
	@Setter
	@Column(name = "ID_VEICULO", length = 64)
	private String veiculoId;

	@Getter
	@Setter
	@Column(name = "ID_CONDUTOR", length = 64)
	private String condutorId;

	@Getter
	@Setter
	@Column(name = "LINHA_AFERIDA", length = 256)
	// Este campo também existe no permissionário, entretanto, o infrator poderá
	// estar operando uma linha ilegal. Este campo será usado para registrar essas
	// situação.
	private String linhaAferida;

	@Getter
	@Setter
	@Column(name = "ID_AGENTE", length = 64)
	private String agenteId;

	@Getter
	@Setter
	@Embedded
	private TipificacaoAit tipificacao;

	@Getter
	@Setter
	@Column(name = "DATA_HOME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataHora;

	@Getter
	@Setter
	@Embedded
	private EmbeddedEndereco local;

	@Getter
	@Setter
	@Column(name = "NOTAS_AGENTE", length = 2048)
	private String notasAgente;

	@Getter
	@Setter
	@Embedded
	private EmbeddedCoordenadas coordenadas;

	@Getter
	@Setter
	@Embedded
	private PermissionarioAit permissionario;

	@Getter
	@Setter
	@Embedded
	private VeiculoAit veiculo;

	@Getter
	@Setter
	@Embedded
	private CondutorAit condutor;

	@Getter
	@Setter
	@Embedded
	private Afericao afericao;

	@Getter
	@Setter
	@ElementCollection(targetClass = String.class)
	@CollectionTable(name = "JT_JPT_AUTUACAO_GALERIA", joinColumns = @JoinColumn(name = "ID"))
	@Column(name = "ID_ITEM", length = 64)
	private Set<String> galeria = new HashSet<String>();

	@Getter
	@Setter
	@Column(name = "ID_IMG_IMPRESSAO", length = 64)
	private String imagemImpressaoId;

}