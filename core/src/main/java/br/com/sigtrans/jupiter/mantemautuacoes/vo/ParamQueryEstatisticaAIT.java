package br.com.sigtrans.jupiter.mantemautuacoes.vo;

import java.util.Date;

import br.com.bristech.msf.vo.CoreVo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class ParamQueryEstatisticaAIT extends CoreVo {

	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	private Date dtInicio;

	@Getter
	@Setter
	private Date dtFinal;

}
