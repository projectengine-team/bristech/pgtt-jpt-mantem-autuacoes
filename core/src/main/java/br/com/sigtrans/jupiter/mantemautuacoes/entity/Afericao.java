package br.com.sigtrans.jupiter.mantemautuacoes.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import br.com.bristech.msf.entity.EmbeddedEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Embeddable
@EqualsAndHashCode(callSuper = true)
public class Afericao extends EmbeddedEntity {
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	@Column(name = "AF_ID_INSTRUMENTO", length = 64)
	private String instrumentoId;

	@Getter
	@Setter
	@Column(name = "AF_MEDIDA_REALIZADA", length = 64)
	private String medidaRealizada;

	@Getter
	@Setter
	@Column(name = "AF_LIMITE_REGULAMENTADO", length = 64)
	private String limiteRegulamentado;

	@Getter
	@Setter
	@Column(name = "AF_LIMITE_CONSIDERADO", length = 64)
	private String limiteConsiderado;

}