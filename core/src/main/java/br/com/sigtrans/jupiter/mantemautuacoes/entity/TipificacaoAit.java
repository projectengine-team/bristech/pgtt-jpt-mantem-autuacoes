package br.com.sigtrans.jupiter.mantemautuacoes.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import br.com.bristech.msf.entity.EmbeddedEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Embeddable
@EqualsAndHashCode(callSuper = true)
public class TipificacaoAit extends EmbeddedEntity {
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	@Column(name = "ID_TIPIFICACAO", length = 64)
	private String tipificacaoId;

	@Getter
	@Setter
	@Column(name = "AMPARO_LEGAL_CODIGO", length = 100)
	private String ampLegalCod;

	@Getter
	@Setter
	@Column(name = "AMPARO_LEGAL_ARTIGO", length = 100)
	private String ampLegalArtigo;

	@Getter
	@Setter
	@Column(name = "AMPARO_LEGAL_LEI", length = 150)
	private String ampLegalLei;

	@Getter
	@Setter
	@Column(name = "GRAVIDADE_PESO")
	private Integer gravidadePeso;

	@Getter
	@Setter
	@Column(name = "GRAVIDADE_PONTOS")
	private Integer gravidadePontos;

	@Getter
	@Setter
	@Column(name = "VALOR_INFRAÇÃO")
	private Double valorInfracao;

	@Getter
	@Setter
	@Column(name = "AMPARO_LEGAL_DESCRICAO", length = 500)
	private String ampLegalDescricao;

	@Getter
	@Setter
	@Column(name = "ID_INST_AFERICAO", length = 64)
	private String instAfericaoId;
	
	

}