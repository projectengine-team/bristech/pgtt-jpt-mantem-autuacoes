package br.com.sigtrans.jupiter.mantemautuacoes.entity;

import javax.persistence.Embeddable;

import br.com.sigtrans.common.entity.AbstractVeiculo;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString
@Embeddable
@EqualsAndHashCode(callSuper = true)
public class VeiculoAit extends AbstractVeiculo {
	private static final long serialVersionUID = 1L;

	

}