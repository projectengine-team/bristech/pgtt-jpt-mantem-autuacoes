package br.com.sigtrans.jupiter.mantemautuacoes.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.bristech.msg.enums.DetailedEnum;
import lombok.Getter;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum StatusAutuacao implements DetailedEnum {
	LAVRADO("Lavrado"), ENVIADO("Enviado para Processamento"), CANCELADO("Cancelado");

	@Getter
	private final String detail;

	private StatusAutuacao(String detail) {
		this.detail = detail;
	}

	public String toString() {
		return this.detail;
	}
}
