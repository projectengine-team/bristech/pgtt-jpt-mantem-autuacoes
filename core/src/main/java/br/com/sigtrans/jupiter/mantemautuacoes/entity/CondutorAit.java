package br.com.sigtrans.jupiter.mantemautuacoes.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import br.com.bristech.msf.entity.EmbeddedEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Embeddable
@EqualsAndHashCode(callSuper = true)
public class CondutorAit extends EmbeddedEntity {
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	@Column(name = "CND_NOME", length = 128)
	private String nomeCondutor;

	@Getter
	@Setter
	@Column(name = "CND_CNH", length = 16)
	private String cnh;

	@Getter
	@Setter
	@Column(name = "CND_CPF", length = 11)
	private String cpf;

}