package br.com.sigtrans.jupiter.mantemautuacoes.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import br.com.bristech.msf.entity.EmbeddedEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Embeddable
@EqualsAndHashCode(callSuper = true)
public class PermissionarioAit extends EmbeddedEntity {
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	@Column(name = "ID_AIT_PERMISSAO", length = 64)
	private String permissaoAitId;

	@Getter
	@Setter
	@Column(name = "NOME_PERMISSIONARIO", length = 128)
	private String nomePermissionario;

	@Getter
	@Setter
	@Column(name = "NR_LINHA", length = 6)
	private String numLinha;

	@Getter
	@Setter
	@Column(name = "NOME_LINHA", length = 128)
	private String nomeLinha;

	@Getter
	@Setter
	@Column(name = "UF_PERMISSIONARIO", length = 2)
	private String uf;

	@Getter
	@Setter
	@Column(name = "ID_TP_PERMISSAO")
	private String tipoPermissaoId;

}