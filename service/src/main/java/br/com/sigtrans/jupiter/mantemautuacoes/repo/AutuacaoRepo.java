package br.com.sigtrans.jupiter.mantemautuacoes.repo;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.bristech.msf.common.vo.SimpleKeyValue;
import br.com.bristech.msf.repository.CrudRepo;
import br.com.sigtrans.jupiter.mantemautuacoes.entity.Autuacao;

public interface AutuacaoRepo extends CrudRepo<Autuacao, String> {

	@Query(value = "SELECT new br.com.bristech.msf.common.vo.SimpleKeyValue(aut.tipificacao.tipificacaoId, count(aut.tipificacao.tipificacaoId)) "
			+ "FROM Autuacao aut where aut.dataHora between :dtInicio and :dtFinal group by aut.tipificacao.tipificacaoId")
	public List<SimpleKeyValue<String, Long>> geraEstatisticaAitPorTipificacao(@Param("dtInicio") Date dtInicio,
			@Param("dtFinal") Date dtFinal);
}
