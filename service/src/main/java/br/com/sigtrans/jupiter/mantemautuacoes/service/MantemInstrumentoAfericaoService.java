package br.com.sigtrans.jupiter.mantemautuacoes.service;

import org.springframework.stereotype.Service;

import br.com.bristech.msf.service.CrudService;
import br.com.sigtrans.jupiter.mantemautuacoes.entity.InstrumentoAfericao;
import br.com.sigtrans.jupiter.mantemautuacoes.repo.InstrumentoAfericaoRepo;

@Service
public class MantemInstrumentoAfericaoService extends CrudService<InstrumentoAfericao, InstrumentoAfericaoRepo> {

}