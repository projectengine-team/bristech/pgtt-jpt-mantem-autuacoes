package br.com.sigtrans.jupiter.mantemautuacoes.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import br.com.bristech.msf.common.vo.SimpleKeyValue;
import br.com.bristech.msf.repository.ObjectNotFoundException;
import br.com.bristech.msf.repository.SaveEntityException;
import br.com.bristech.msf.service.CrudService;
import br.com.sigtrans.jupiter.mantemautuacoes.entity.Autuacao;
import br.com.sigtrans.jupiter.mantemautuacoes.repo.AutuacaoRepo;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MantemAutuacoesService extends CrudService<Autuacao, AutuacaoRepo> {

	/**
	 * Gera as estatisticas das AITs por tipo
	 * 
	 * @param inicio
	 * @return
	 */
	public List<SimpleKeyValue<String, Long>> geraEstatisticaAitPorTipificacao(Date dtInicio, Date dtFinal) {
		return getRepo().geraEstatisticaAitPorTipificacao(dtInicio, dtFinal);
	}
	
	@Override
	public Autuacao save(Autuacao entity) throws SaveEntityException, ObjectNotFoundException {
		log.debug("Registrando autuaçao: {}", entity);
		return super.save(entity);
	}
}