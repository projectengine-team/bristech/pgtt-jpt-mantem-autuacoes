package br.com.sigtrans.jupiter.mantemautuacoes.repo;

import br.com.bristech.msf.repository.CrudRepo;
import br.com.sigtrans.jupiter.mantemautuacoes.entity.InstrumentoAfericao;

public interface InstrumentoAfericaoRepo extends CrudRepo<InstrumentoAfericao, String> {

}
