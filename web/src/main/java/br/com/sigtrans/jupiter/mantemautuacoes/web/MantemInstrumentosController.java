package br.com.sigtrans.jupiter.mantemautuacoes.web;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.bristech.msf.repository.DuplicateObjectException;
import br.com.bristech.msf.repository.ObjectNotFoundException;
import br.com.bristech.msf.repository.SaveEntityException;
import br.com.bristech.web.BaseController;
import br.com.bristech.web.BusinessException;
import br.com.bristech.web.ConflictException;
import br.com.bristech.web.NotFoundException;
import br.com.sigtrans.jupiter.mantemautuacoes.entity.InstrumentoAfericao;
import br.com.sigtrans.jupiter.mantemautuacoes.service.MantemInstrumentoAfericaoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/mantem-instrumentos")
@Api("Mantem Instrumentos de Afericao")
public class MantemInstrumentosController extends BaseController {

	@Autowired
	private MantemInstrumentoAfericaoService service;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces="application/json")
	@ApiOperation("Retorna os dados com base no ID passado.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retorna um registro existente."),
			@ApiResponse(code = 403, message = "Sem permissão para acessar este recurso."),
			@ApiResponse(code = 404, message = "O registro não foi encontrado."),
			@ApiResponse(code = 500, message = "Ocorreu um erro interno."), })
	public InstrumentoAfericao findInstrumentoAfericaoById(
			@ApiParam(value = "ID do registro", required = true) @PathVariable String id,
			@ApiParam(hidden = true) @RequestHeader Map<String, String> headers) {
		try {
			return this.service.findByPrimaryKey(id);
		} catch (ObjectNotFoundException e) {
			throw new NotFoundException();
		}
	}

	@RequestMapping(value = "/", method = RequestMethod.POST, produces="application/json", consumes = "application/json")
	@ApiOperation("Insere um novo registro no banco com base nos dados passados como parâmetro.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retorna o novo registro inserido."),
			@ApiResponse(code = 403, message = "Sem permissão para realizar esta operação."),
			@ApiResponse(code = 409, message = "Um registro com o mesmo ID já existe."),
			@ApiResponse(code = 500, message = "Ocorreu um erro interno."), })
	public InstrumentoAfericao newInstrumentoAfericao(
			@ApiParam(value = "Dados a serem inseridos.", required = true) @RequestBody InstrumentoAfericao data,
			@ApiParam(hidden = true) @RequestHeader Map<String, String> headers) {
		try {
			return this.service.create(data);
		} catch (DuplicateObjectException e) {
			throw new ConflictException(e.getMessage());
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces="application/json", consumes = "application/json")
	@ApiOperation("Atualiza os dados de um registro existente com base no ID e dados passados como parâmetro.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retorna registro com os dados atualizados."),
			@ApiResponse(code = 400, message = "Dados inválidos foram submetidos."),
			@ApiResponse(code = 403, message = "Sem permissão para realizar esta operação."),
			@ApiResponse(code = 404, message = "O registro que se deseja atualizar não existe na base."),
			@ApiResponse(code = 500, message = "Ocorreu um erro interno."), })
	public InstrumentoAfericao updateInstrumentoAfericao(
			@ApiParam(value = "ID do registro ao qual se deseja atualizar", required = true) @PathVariable String id,
			@ApiParam(value = "Dados a serem atualizados", required = true) @RequestBody InstrumentoAfericao data,
			@ApiParam(hidden = true) @RequestHeader Map<String, String> headers) {
		try {
			return this.service.save(data);
		} catch (SaveEntityException e) {
			throw new BusinessException(e.getMessage());
		} catch (ObjectNotFoundException e) {
			throw new NotFoundException(e.getMessage());
		}
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST, produces="application/json", consumes = "application/json")
	@ApiOperation("Pesquisa um registro no banco de dados com base nos dados passados como parâmetro.")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Retorna uma lista de registros que satisfazem a regra."),
			@ApiResponse(code = 403, message = "Sem permissão para realizar esta operação."),
			@ApiResponse(code = 500, message = "Ocorreu um erro interno."), })
	public List<InstrumentoAfericao> searchInstrumentoAfericao(
			@ApiParam(value = "Critérios de pesquisa.", required = true) @RequestBody InstrumentoAfericao criterias,
			@ApiParam(hidden = true) @RequestHeader Map<String, String> headers) {
		return this.service.find(criterias);
	}
}
