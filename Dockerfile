FROM maven:3-openjdk-11

ARG DIR_TARGET

WORKDIR /opt/sigtrans

COPY ${DIR_TARGET}/*.jar /opt/sigtrans/app.jar

EXPOSE 8080

ENTRYPOINT ["java","-jar","/opt/sigtrans/app.jar"]