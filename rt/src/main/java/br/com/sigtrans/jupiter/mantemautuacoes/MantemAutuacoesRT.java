package br.com.sigtrans.jupiter.mantemautuacoes;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
@ComponentScan({ "br.com.sigtrans.jupiter.mantemautuacoes" })
@EntityScan(basePackages = { "br.com.sigtrans.jupiter.mantemautuacoes" })
@EnableJpaRepositories(basePackages = { "br.com.sigtrans.jupiter.mantemautuacoes" })
public class MantemAutuacoesRT {

	@Autowired
	private Environment environment;

	@PersistenceContext
	private EntityManager entityManager;

	public static void main(String[] args) {
		SpringApplication.run(MantemAutuacoesRT.class, args);
	}

	@PostConstruct
	public void init() {
		log.info("br.com.sigtrans.jupiter.mantemautuacoes Iniciado.");
		for (String profileName : environment.getActiveProfiles()) {
			log.info("Ambiente de execução: {}", profileName);
		}

	}

	@Bean
	public PlatformTransactionManager transactionManager() {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManager.getEntityManagerFactory());
		return transactionManager;
	}
}
